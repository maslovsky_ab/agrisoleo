import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Crops {

    public void makeNewCrop(WebDriver driver, WebDriverWait wait) {
        //driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Crops']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        driver.findElement(By.xpath("//input[@placeholder='Crop Name']")).sendKeys("Crop1");
        driver.findElement(By.xpath("//div[@class='styled_labelContainer__TKpiP']//span[text()='Add new Period']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Add new period']")));
        driver.findElement(By.xpath("//label[text()='Period Name']/..//input[@name='name']")).sendKeys("Period1");
        driver.findElement(By.xpath("//input[@name='crop_height']")).sendKeys("10");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[1]")).sendKeys("2022-01-01");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[2]")).sendKeys("2022-01-02");
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Add']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Period1']")));
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Crop1']")));
    }

    public boolean newCropCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//p[text()='Crop1']")).isDisplayed();
        if (result == true) {
            driver.findElement(By.xpath("(//p[text()='Crop1']/..//..//button[@type='button'])[1]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='CropsEdit_button__l43Ys']")));
        }
        return (result &&
                driver.findElement(By.xpath("(//td)[1]")).getText().equals("Period1") &&
                // "Crop Height" покашо не работает
                // driver.findElement(By.xpath("(//td)[2]")).getText().equals("10") &&
                driver.findElement(By.xpath("(//td)[3]")).getText().equals("2022-01-01") &&
                driver.findElement(By.xpath("(//td)[4]")).getText().equals("2022-01-02"));
    }

    public void editCrop(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//p[text()='Crop1']/..//..//button[@type='button'])[1]")));
        driver.findElement(By.xpath("(//p[text()='Crop1']/..//..//button[@type='button'])[1]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='CropsEdit_button__l43Ys']")));
        driver.findElement(By.xpath("//button[@class='CropsEdit_button__l43Ys']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Edit period']")));
        driver.findElement(By.xpath("//label[text()='Period Name']/..//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), "Period2");
        driver.findElement(By.xpath("//input[@name='crop_height']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), "20");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[1]")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "2022-01-02");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[2]")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "2022-01-03");
// должно быть Save, но пока что Add
// driver.findElement(By.xpath("//button[@type='submit']//span[text()='Save']")).click();
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Add']")).click();

        driver.findElement(By.xpath("//input[@placeholder='Crop Name']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), "Crop2");
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Crop2']")));
    }

    public boolean editCropCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//p[text()='Crop2']")).isDisplayed();
        System.out.println(driver.findElement(By.xpath("//p[text()='Crop2']")).isDisplayed());
        if (result == true) {
            driver.findElement(By.xpath("(//p[text()='Crop2']/..//..//button[@type='button'])[1]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='CropsEdit_button__l43Ys']")));
        }
        return (result &&
                driver.findElement(By.xpath("(//td)[1]")).getText().equals("Period2") &&
                // "Crop Height" покашо не работает
                // driver.findElement(By.xpath("(//td)[2]")).getText().equals("20") &&
                driver.findElement(By.xpath("(//td)[3]")).getText().equals("2022-01-02") &&
                driver.findElement(By.xpath("(//td)[4]")).getText().equals("2022-01-03"));
    }

    public boolean newCropErrorShowCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Crops']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Save']")).click();
        boolean result = (driver.findElement(By.xpath("//input[@placeholder='Crop Name']/../span[text()='This field is required']")).isDisplayed() &&
                driver.findElement(By.xpath("//table[@class='CropsCreate_table__81pjd']/../span[text()='There must be at least one period.']")).isDisplayed());
        if (result == true) {
            driver.findElement(By.xpath("//input[@placeholder='Crop Name']")).sendKeys("123456789012345678901234567890123456789012345678901");
            result = driver.findElement(By.xpath("//input[@placeholder='Crop Name']/../span[text()='Max length are 50 charts']")).isDisplayed();
        }
        if (result == true) {
            driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']//span[text()='Add new Period']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Add new period']")));
            driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Add']")).click();
            result = (driver.findElement(By.xpath("//input[@name='name']/..//../span[text()='This field is required']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Starting Point']/..//../span[text()='This field is required']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Ending Point']/..//../span[text()='This field is required']")).isDisplayed());
        }
        if (result == true) {
            driver.findElement(By.xpath("//label[text()='Period Name']/..//input[@name='name']")).sendKeys("123456789012345678901234567890123456789012345678901");
            driver.findElement(By.xpath("(//input[@placeholder='Date'])[1]")).sendKeys("2022-01-03");
            driver.findElement(By.xpath("(//input[@placeholder='Date'])[2]")).sendKeys("2022-01-02");
            driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Add']")).click();
            result = (driver.findElement(By.xpath("//input[@name='name']/..//../span[text()='Max length are 50 charts']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Starting Point']/..//../span[text()='Invalid date range']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Ending Point']/..//../span[text()='Invalid date range']")).isDisplayed());
        }
        driver.findElement(By.xpath("//div[@class='AddModalPeriodCropsTable_row__ehwat']//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Leave page']")).click();
        return (result);
    }

    public boolean editCropErrorShowCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//div[@class='styled_flex__kd18m']//button[@type='button']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='CropsEdit_button__l43Ys']")));
        driver.findElement(By.xpath("//button[@class='CropsEdit_button__l43Ys']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Edit period']")));
        driver.findElement(By.xpath("//div[@class='styled_inputContainer__0Gnhd']//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='dateStart']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='dateEnding']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE));
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Add']")).click();
        boolean result = (driver.findElement(By.xpath("//input[@name='name']/..//../span[text()='This field is required']")).isDisplayed() &&
                driver.findElement(By.xpath("//label[text()='Starting Point']/..//../span[text()='This field is required']")).isDisplayed() &&
                driver.findElement(By.xpath("//label[text()='Ending Point']/..//../span[text()='This field is required']")).isDisplayed());
        if (result == true) {
            driver.findElement(By.xpath("//div[@class='styled_inputContainer__0Gnhd styled_errorBorder__LomfX']//input[@name='name']")).sendKeys("123456789012345678901234567890123456789012345678901");
            driver.findElement(By.xpath("(//input[@placeholder='Date'])[1]")).sendKeys("2022-01-03");
            driver.findElement(By.xpath("(//input[@placeholder='Date'])[2]")).sendKeys("2022-01-02");
            result = (driver.findElement(By.xpath("//input[@name='name']/..//../span[text()='Max length are 50 charts']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Starting Point']/..//../span[text()='Invalid date range']")).isDisplayed() &&
                    driver.findElement(By.xpath("//label[text()='Ending Point']/..//../span[text()='Invalid date range']")).isDisplayed());
        }
        driver.findElement(By.xpath("//div[@class='AddModalPeriodCropsTable_row__ehwat']//span[text()='Cancel']")).click();
        if (result == true) {
            driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), Keys.TAB);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//tbody//button)[2]")));
            driver.findElement(By.xpath("(//tbody//button)[2]")).click();
            driver.findElement(By.xpath("//div[@class='CropsEdit_flex__KR7wN']//button[@type='submit']")).click();
            result = (driver.findElement(By.xpath("//input[@placeholder='Crop Name']/../span[text()='This field is required']")).isDisplayed() &&
                    driver.findElement(By.xpath("//table[@class='CropsEdit_table__viVsX']/../span[text()='There must be at least one period.']")).isDisplayed());
        }
        if (result == true) {
            driver.findElement(By.xpath("//input[@name='name']")).sendKeys("123456789012345678901234567890123456789012345678901");
            result = driver.findElement(By.xpath("//input[@placeholder='Crop Name']/../span[text()='Max length are 50 charts']")).isDisplayed();
        }
        driver.findElement(By.xpath("//div[@class='CropsEdit_flex__KR7wN']//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Leave page']")).click();
        return (result);
    }

    public boolean duplicateCropCheck(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        String cropName = driver.findElement(By.xpath("//p[@class='styled_name__FmpdA']")).getText();
        driver.findElement(By.xpath("(//button[@class='styled_button__3u9ty'])[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        boolean result = driver.findElement(By.xpath("(//p[@class='styled_name__FmpdA'])[2]")).getText().equals(cropName);

        if (result == true) {
        driver.findElement(By.xpath("(//button[@class='styled_button__3u9ty'])[1]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//td"), 1));
        String periodName = driver.findElement(By.xpath("(//td)[1]")).getText();
        String cropHeight = driver.findElement(By.xpath("(//td)[2]")).getText();
        String startingPoint = driver.findElement(By.xpath("(//td)[3]")).getText();
        String endingPoint = driver.findElement(By.xpath("(//td)[4]")).getText();
        driver.findElement(By.xpath("//span[text()='Cancel']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        driver.findElement(By.xpath("(//button[@class='styled_button__3u9ty'])[3]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//td"), 1));
            result = (driver.findElement(By.xpath("(//td)[1]")).getText().equals(periodName) &&
                driver.findElement(By.xpath("(//td)[2]")).getText().equals(cropHeight) &&
                driver.findElement(By.xpath("(//td)[3]")).getText().equals(startingPoint) &&
                driver.findElement(By.xpath("(//td)[4]")).getText().equals(endingPoint));
            driver.findElement(By.xpath("//span[text()='Cancel']")).click();
            wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        }
        return result;
    }

    public boolean deleteCropCheck(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("(//button[@class='styled_button__3u9ty styled_red__zocII'])[1]")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        return (driver.findElements(By.xpath("//div[@class='styled_container__vhG6v']")).size() == 0);
    }

    public boolean deletePeriodNewCropCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Crops']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();

        driver.findElement(By.xpath("//div[@class='styled_labelContainer__TKpiP']//span[text()='Add new Period']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Add new period']")));
        driver.findElement(By.xpath("//label[text()='Period Name']/..//input[@name='name']")).sendKeys("Period1");
        driver.findElement(By.xpath("//input[@name='crop_height']")).sendKeys("10");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[1]")).sendKeys("2022-01-01");
        driver.findElement(By.xpath("(//input[@placeholder='Date'])[2]")).sendKeys("2022-01-02");
        driver.findElement(By.xpath("//button[@type='submit']//span[text()='Add']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Period1']")));
        driver.findElement(By.xpath("(//td[text()='Period1']/..//button)[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//tr"), 2));
        boolean result = (driver.findElements(By.xpath("//tr")).size() == 1);
        driver.findElement(By.xpath("//span[text()='Cancel']")).click();
        return result;
    }

    public boolean deletePeriodExistingCropCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@class='styled_button__3u9ty']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Period1']")));
        driver.findElement(By.xpath("(//td[text()='Period1']/..//button)[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//tr"), 2));
        boolean result = (driver.findElements(By.xpath("//tr")).size() == 1);
        driver.findElement(By.xpath("//span[text()='Cancel']")).click();
        driver.findElement(By.xpath("//span[text()='Leave page']")).click();
        return result;
    }

}