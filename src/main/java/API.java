import org.apache.commons.io.IOUtils;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class API {

    public JSONObject login() throws JSONException {
        JSONObject requestBody = new JSONObject();
        requestBody.put("email", "maslovsky_ab@groupbwt.com");
        requestBody.put("password", "Alex_123456");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(requestBody.toString());
        JSONObject response = new JSONObject(request.post("https://api.cryptometric.com/api/login").asString());
        return (response);
    }

    public String getAlertsList() throws IOException, JSONException {
        URL url = new URL("https://api.cryptometric.com/api/alerts");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Authorization", "Bearer "+ login().getString("token"));
        InputStream in = http.getInputStream();
        String body = IOUtils.toString(in);
        System.out.println(body);
        return (body);
    }

    public JSONObject getNews() throws IOException, JSONException {
        URL url = new URL("https://api.cryptometric.com/api/news");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Authorization", "Bearer "+ login().getString("token"));
        InputStream in = http.getInputStream();
        JSONObject body = new JSONObject(IOUtils.toString(in));
        return (body);
    }

    public Integer getCryptoMarket() throws IOException, JSONException {
        URL url = new URL("https://api.cryptometric.com/api/crypto-markets?length=10&start=0&&search=&order[column]=rank&order[dir]=asc");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Authorization", "Bearer "+ login().getString("token"));
        InputStream in = http.getInputStream();
        JSONObject body = new JSONObject(IOUtils.toString(in));
        return (Integer.valueOf(body.getString("recordsFiltered")));
    }
}
