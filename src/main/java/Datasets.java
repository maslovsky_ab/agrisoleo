import com.codeborne.selenide.selector.ByText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class Datasets {

    public void makeNewWeatherDataset(WebDriver driver, WebDriverWait wait) {
        //driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Datasets']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("weather");
        driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']")).click();
        driver.findElement(new ByText("Weather")).click();
        driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__value-container select__value-container--has-value css-1olco0v']")).click();
        driver.findElement(new ByText("1 hour")).click();
        driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']")).click();
        driver.findElement(new ByText("Etc/GMT0")).click();
        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("C:\\Users\\Administrator\\Desktop\\Datasets.xlsx");
        File file = new File("src/main/resources/Datasets.xlsx");
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys(file.getAbsolutePath());
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='weather']")));
    }

    public boolean newWeatherDatasetCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='weather']")).isDisplayed();
        if (result == true) {
            driver.findElement(By.xpath("//h3[text()='Electrical Production Datasets']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset File']/..//p[text()='Datasets']")));
            result = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Weather") &&
                    driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("1 hour") &&
                    driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Etc/GMT+0") &&
                    driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText().equals("Datasets");
        }
        return result;
    }

    public void makeNewProductionDataset(WebDriver driver, WebDriverWait wait) {
        //driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Datasets']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("prod");
        driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']")).click();
        driver.findElement(new ByText("Production")).click();
        driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__control css-11xodl7-control']")).click();
        driver.findElement(new ByText("1 hour")).click();
        driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']")).click();
        driver.findElement(new ByText("Etc/GMT0")).click();
        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("C:\\Users\\Administrator\\Desktop\\Datasets.xlsx");
        File file = new File("src/main/resources/Datasets.xlsx");
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys(file.getAbsolutePath());
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Electrical Production Datasets']/..//p[text()='prod']")));
    }

    public boolean newProductionDatasetCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//h3[text()='Electrical Production Datasets']/..//p[text()='prod']")).isDisplayed();
        if (result == true) {
            driver.findElement(By.xpath("//h3[text()='Electrical Production Datasets']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset File']/..//p[text()='Datasets']")));
            result = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Production") &&
                    driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("1 hour") &&
                    driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Etc/GMT+0") &&
                    driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText().equals("Datasets");
        }
        return result;
    }

    public void makeNewBlankDataset(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Datasets']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//span[text()='Save']")).click();
    }

    public boolean newBlankDatasetCheck(WebDriver driver, WebDriverWait wait) {
        boolean result =
            (driver.findElement(By.xpath("//input[@name='name']/../span[text()='This field is required']")).isDisplayed() &&
            driver.findElement(By.xpath("//label[text()='Dataset type']/../span[text()='This field is required']")).isDisplayed() &&
            driver.findElement(By.xpath("//label[text()='Timezone']/../span[text()='This field is required']")).isDisplayed() &&
            driver.findElement(By.xpath("//label[text()='Dataset File']/..//span[text()='Dataset file is required']")).isDisplayed());
        if (result == true){
            driver.findElement(By.xpath("//input[@name='name']")).sendKeys("123456789012345678901234567890123456789012345678901");
            result = driver.findElement(By.xpath("//input[@name='name']/../span[text()='Max length are 50 charts']")).isDisplayed();
        }
        driver.findElement(By.xpath("//button[@type='button']//span[text()='Cancel']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Leave page']")).click();
        return result;
    }

    public void makeNewDatasetIncorrectExcel(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")));
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Datasets']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("weather");
        driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']")).click();
        driver.findElement(new ByText("Weather")).click();
        driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__control css-11xodl7-control']")).click();
        driver.findElement(new ByText("1 hour")).click();
        driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']")).click();
        driver.findElement(new ByText("Etc/GMT+0")).click();
        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("C:\\Users\\Administrator\\Desktop\\Datasets.xlsx");
        File file = new File("src/main/resources/IncorrectDatasets.xlsx");
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys(file.getAbsolutePath());
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Error']")));
    }

    public boolean makeNewDatasetIncorrectExcelCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//h3[text()='Error']")).isDisplayed();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']//span[text()='Leave page']")).click();
        return result;
    }

    public void editWeatherDataset(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")));
        driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));

        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "prod");
//        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("weather");

        driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).click();
        driver.findElement(new ByText("Production")).click();
//        driver.findElement(new ByText("Weather")).click();

//        driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__value-container select__value-container--has-value css-whkble']")).click();
//        driver.findElement(new ByText("5 minutes")).click();
//        driver.findElement(new ByText("1 hour")).click();

        driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']")).click();
        driver.findElement(new ByText("Etc/GMT+1")).click();
        //driver.findElement(new ByText("Etc/GMT+0")).click();

        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("C:\\Users\\Administrator\\Desktop\\Datasets.xlsx");
        driver.findElement(By.xpath("//button[@class='DatasetModel_delete-file__+xNa9']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Drop your Excel file to upload']")));
        File file = new File("src/main/resources/Datasets4replace.xlsx");
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys(file.getAbsolutePath());
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='prod']")));
    }

    public boolean editWeatherDatasetCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='prod']")).isDisplayed();
        if (result == true) {
            driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
            result = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Production") &&
                    //driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-qc6sy-singleValue']")).getText().equals("1 hour") &&
                    driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Etc/GMT+1") &&
                    driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText().equals("Datasets4replace");
        }
        return result;
    }

    public void editProductionDataset(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")));
        driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "weather");

        driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).click();
        driver.findElement(new ByText("Weather")).click();

        driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']")).click();
        driver.findElement(new ByText("Etc/GMT+1")).click();
        //driver.findElement(new ByText("Etc/GMT+0")).click();

        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("C:\\Users\\Administrator\\Desktop\\Datasets.xlsx");
        driver.findElement(By.xpath("//button[@class='DatasetModel_delete-file__+xNa9']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Drop your Excel file to upload']")));
        File file = new File("src/main/resources/Datasets4replace.xlsx");
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys(file.getAbsolutePath());
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='weather']")));
    }

    public boolean editProductionDatasetCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//p[text()='weather']")).isDisplayed();
        if (result == true) {
            driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
            result = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Weather") &&
                    //driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-qc6sy-singleValue']")).getText().equals("1 hour") &&
                    driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("Etc/GMT+1") &&
                    driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText().equals("Datasets4replace");
        }
        return result;
    }

    public boolean editDatasetMessagesCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Dataset type']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE));
        driver.findElement(By.xpath("//button[@class='DatasetModel_delete-file__+xNa9']")).click();
        ///какого надо 2 раза нажимать?!
        driver.findElement(By.xpath("//button[@class='DatasetModel_delete-file__+xNa9']")).click();
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='name']/..//span[text()='This field is required']")));
        boolean result = driver.findElement(By.xpath("//input[@name='name']/..//span[text()='This field is required']")).isDisplayed() &&
                driver.findElement(By.xpath("//span[text()='Dataset file is required']")).isDisplayed();
        driver.findElement(By.xpath("//span[text()='Cancel']")).click();
        driver.findElement(By.xpath("//span[text()='Leave page']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Electrical Production Datasets']/..//p[text()='prod']")));
        return result;
    }

    public boolean duplicateDatasetCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("(//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx'])[2]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='DatasetsCard_container__2q18I']"))).getSize().equals(2);
        boolean result = driver.findElement(By.xpath("(//div[@class='DatasetsCard_container__2q18I']//p)[1]")).getText().equals
                (driver.findElement(By.xpath("(//div[@class='DatasetsCard_container__2q18I']//p)[2]")).getText());
        if (result == true) {
            driver.findElement(By.xpath("(//button[@class='DatasetsCard_button__1nmIx'])[1]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Datasets']")));
            String datasetType = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
            String samplingRate = driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
            String timezone = driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
            String datasetName = driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText();
            System.out.println(datasetType+" // "+samplingRate+" // "+timezone+" // " +datasetName);
            driver.findElement(By.xpath("//span[text()='Cancel']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='DatasetsCard_container__2q18I']"))).getSize().equals(2);
            driver.findElement(By.xpath("(//button[@class='DatasetsCard_button__1nmIx'])[3]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Datasets']")));
            result = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals(datasetType) &&
                    driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals(samplingRate) &&
                    driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals(timezone) &&
                    driver.findElement(By.xpath("//label[text()='Dataset File']/..//p")).getText().equals(datasetName);
        }
        return result;
    }

    public boolean deleteDatasetCheck(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//button[@class='DatasetsCard_button__1nmIx DatasetsCard_red__zQUj7']")).click();
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[@class='DatasetsCard_container__2q18I']"))));
        return driver.findElements(By.xpath("//div[@class='DatasetsCard_container__2q18I']")).size() == 0;
    }

}