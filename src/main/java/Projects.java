import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Projects {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments()).create();
    //WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

    public void makeNewProject(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@placeholder='Enter name']")).sendKeys("autoTest Project new");
        driver.findElement(By.xpath("//input[@placeholder='Latitude']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@placeholder='Longitude']")).sendKeys("2");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest Project new']")));
        //return driver.findElement(By.xpath("//a[text()='autoTest Project new']")).isDisplayed();
    }

    public boolean makeNewProjectCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = false;
        if (driver.findElement(By.xpath("//a[text()='autoTest Project new']")).isDisplayed()) { result = true; }
        else {result = false;}
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[text()='Edit project']")));
        if (driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("autoTest Project new") &&
                driver.findElement(By.xpath("//input[@placeholder='Latitude']")).getAttribute("defaultValue").equals("1") &&
                driver.findElement(By.xpath("//input[@placeholder='Longitude']")).getAttribute("defaultValue").equals("2") &&
                result == true) {result = true;}
        else result = false;
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        return result;
    }

    public boolean makeNewProjectBlankNameFieldCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Project name']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean makeNewProjectBlankLatitudeFieldCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Latitude']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean makeNewProjectBlankLongitudeFieldCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Longitude']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean makeNewProjectLatitudeLess90Check(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@placeholder='Latitude']")).sendKeys("-91");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Minimum value is -90']")).isDisplayed();
    }

    public boolean makeNewProjectLatitudeMore90Check(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys("91");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Maximum value is 90']")).isDisplayed();
    }

    public boolean makeNewProjectLongitudeLess180Check(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@placeholder='Longitude']")).sendKeys("-181");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Minimum value is -180']")).isDisplayed();
    }

    public boolean makeNewProjectLongitudeMore180Check(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys("181");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Maximum value is 180']")).isDisplayed();
    }

    public boolean editProjectCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = false;
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys("1");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest Project new1']")));
        if (driver.findElement(By.xpath("//a[text()='autoTest Project new1']")).isDisplayed() == true) result = true;
        driver.findElement(By.xpath("//a[text()='autoTest Project new1']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[text()='Edit project']")));
        if (driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("autoTest Project new1") &&
                driver.findElement(By.xpath("//input[@placeholder='Latitude']")).getAttribute("defaultValue").equals("11") &&
                driver.findElement(By.xpath("//input[@placeholder='Longitude']")).getAttribute("defaultValue").equals("21") &&
                result == true) {result = true;}
        else result = false;
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        return result;
    }

    public boolean editProjectBlankNameFieldCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Project name']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean editProjectBlankLatitudeFieldCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Latitude']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean editProjectBlankLongitudeFieldCheck(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Longitude']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean editProjectLatitudeLess90Check(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//input[@placeholder='Latitude']")).sendKeys("-91");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Minimum value is -90']")).isDisplayed();
    }

    public boolean editProjectLatitudeMore90Check(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//input[@name='lat']")).sendKeys("91");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Maximum value is 90']")).isDisplayed();
    }

    public boolean editProjectLongitudeLess180Check(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//input[@placeholder='Longitude']")).sendKeys("-181");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Minimum value is -180']")).isDisplayed();
    }

    public boolean editProjectLongitudeMore180Check(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.chord(Keys.CONTROL, "a"), "1");
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//input[@name='long']")).sendKeys("181");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//span[text()='Maximum value is 180']")).isDisplayed();
    }

    public boolean deleteProjectCheck(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//a[text()='autoTest Project new']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Delete']")).click();
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//a[text()='autoTest Project new']"))));
        return driver.findElements(By.xpath("//a[text()='autoTest1']")).size() == 0;
    }

}