import com.codeborne.selenide.selector.ByText;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import static java.io.File.separator;

public class Batch {
    String panelHeight, panelOpacity, azimuth, panelSizeX, panelSizeY, initialOffsetX, initialOffsetY;
    String panelSize, initialOffset, numberOfPanels, spaceBetweenPanels, fieldSize, structureType, panelTilt;
    String panelTranslation , maximalLateralTranslation, lateralOffset, periodName, startingPoint, endingPoint;
    String weatherDatasetType, weatherSamplingRate, weatherTimezone, prodDatasetType, prodSamplingRate, prodTimezone;
    String numberOfPanelsX, numberOfPanelsY, spaceBetweenPanelsX,spaceBetweenPanelsY, fieldSizeX, fieldSizeY;
    String simulationName, structureName, weatherDatasetName, productionDatasetName, cropName;

    public void makeNewBatch(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Batches']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='30 minutes']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Batch");
        driver.findElement(By.xpath("//label[text()='Base Simulation']/..//input[@class='select__input']")).sendKeys("Sim", Keys.RETURN, Keys.RETURN);

        driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//input[@class='select__input']")).click();
        driver.findElement(new ByText("1 hour")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Parameters of interest']/../div")));
        driver.findElement(By.xpath("//label[text()='Parameters of interest']/../div")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Datasets']")));
        driver.findElement(By.xpath("//h3[text()='Structures']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Panel Height']")));
        driver.findElement(By.xpath("//label[text()='Panel Height']")).click();
        driver.findElement(By.xpath("//label[text()='Panel Opacity']")).click();
        driver.findElement(By.xpath("//label[text()='Azimuth']")).click();
        driver.findElement(By.xpath("//label[text()='Panel Size']")).click();
        driver.findElement(By.xpath("//label[text()='Panel Offset']")).click();
        driver.findElement(By.xpath("//label[text()='Number of Panels']")).click();
        driver.findElement(By.xpath("//label[text()='Space Between Panels']")).click();
        driver.findElement(By.xpath("//label[text()='Field Size']")).click();

        driver.findElement(By.xpath("//h3[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Crop']")));
        driver.findElement(By.xpath("//label[text()='Crop']")).click();

        driver.findElement(By.xpath("//h3[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Weather Dataset']")));
        driver.findElement(By.xpath("//label[text()='Weather Dataset']")).click();
        driver.findElement(By.xpath("//label[text()='Production Dataset']")).click();

        driver.findElement(By.xpath("//span[text()='Add row']/../../../button")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//span[@class='styled_label__gZBu7 styled_isLoading__U86n+']"))));
        driver.findElement(By.xpath("//span[text()='Cancel']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Batch']")));
    }

    public boolean newBatchCheck(WebDriver driver, WebDriverWait wait)  {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='rows.0.panel_height']")));

        simulationName = driver.findElement(By.xpath("//label[text()='Base Simulation']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
        panelHeight = driver.findElement(By.xpath("//input[@name='rows.0.panel_height']")).getAttribute("valueAsNumber");
        panelOpacity = driver.findElement(By.xpath("//input[@name='rows.0.panel_opacity']")).getAttribute("valueAsNumber");
        azimuth = driver.findElement(By.xpath("//input[@name='rows.0.azimuth']")).getAttribute("valueAsNumber");

        panelSizeX = driver.findElement(By.xpath("//input[@name='rows.0.panel_size.x']")).getAttribute("valueAsNumber");
        panelSizeY = driver.findElement(By.xpath("//input[@name='rows.0.panel_size.y']")).getAttribute("valueAsNumber");
        initialOffsetX = driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.x']")).getAttribute("valueAsNumber");
        initialOffsetY = driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.y']")).getAttribute("valueAsNumber");
        numberOfPanelsX = driver.findElement(By.xpath("//input[@name='rows.0.panels_number.x']")).getAttribute("valueAsNumber");
        numberOfPanelsY = driver.findElement(By.xpath("//input[@name='rows.0.panels_number.y']")).getAttribute("valueAsNumber");
        spaceBetweenPanelsX = driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.x']")).getAttribute("valueAsNumber");
        spaceBetweenPanelsY = driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.y']")).getAttribute("valueAsNumber");
        fieldSizeX = driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber");
        fieldSizeY = driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber");
        weatherDatasetName = driver.findElement(By.xpath("//td[9]")).getAttribute("outerText");
        productionDatasetName = driver.findElement(By.xpath("//td[10]")).getAttribute("outerText");
        cropName = driver.findElement(By.xpath("//td[11]")).getAttribute("outerText");

        //////////// читаем данные из Симуляции /////////////////////////////
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='"+simulationName+"']")));
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));
        structureName = driver.findElement(By.xpath("//span[text()='Structures']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
        boolean result =
                driver.findElement(By.xpath("//span[text()='Crops']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().
                        equals(cropName) &&
                driver.findElement(By.xpath("//span[text()='Weather Dataset']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().
                        equals(weatherDatasetName) &&
                driver.findElement(By.xpath("//span[text()='Electrical Production Dataset']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().
                        equals(productionDatasetName);
        if (result == true) {
            driver.findElement(By.xpath("//a[text()='Structures']")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='"+structureName+"']")));
            driver.findElement(By.xpath("//button[@class='styled_button__e6WEB']")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
            result = driver.findElement(By.xpath("//input[@name='panel_height']")).getAttribute("valueAsNumber").equals(panelHeight) &&
                    driver.findElement(By.xpath("//label[text()='Panel Opacity']/..//div[@class='styled_tooltip__i6Njk']")).getText().equals(panelOpacity) &&
                    driver.findElement(By.xpath("//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk']")).getText().equals(azimuth) &&
                    driver.findElement(By.xpath("//input[@name='panel_x']")).getAttribute("valueAsNumber").equals(panelSizeX) &&
                    driver.findElement(By.xpath("//input[@name='panel_y']")).getAttribute("valueAsNumber").equals(panelSizeY) &&
                    driver.findElement(By.xpath("//input[@name='initial_offset_x']")).getAttribute("valueAsNumber").equals(initialOffsetX) &&
                    driver.findElement(By.xpath("//input[@name='initial_offset_y']")).getAttribute("valueAsNumber").equals(initialOffsetY) &&
                    driver.findElement(By.xpath("//input[@name='panels_number_x']")).getAttribute("valueAsNumber").equals(numberOfPanelsX) &&
                    driver.findElement(By.xpath("//input[@name='panels_number_y']")).getAttribute("valueAsNumber").equals(numberOfPanelsY) &&
                    driver.findElement(By.xpath("//input[@name='panels_gap_x']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsX) &&
                    driver.findElement(By.xpath("//input[@name='panels_gap_y']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsY) &&
                    driver.findElement(By.xpath("//input[@name='field_size_x']")).getAttribute("valueAsNumber").equals(fieldSizeX) &&
                    driver.findElement(By.xpath("//input[@name='field_size_y']")).getAttribute("valueAsNumber").equals(fieldSizeY);
        }
        return result;
    }

    public void editBatch(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));

        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Panel Height']")));
        driver.findElement(By.xpath("//input[@name='rows.0.panel_height']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "5");

        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Batch']")));
    }

    public boolean editBatchCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Panel Height']")));
        return(driver.findElement(By.xpath("//input[@name='rows.0.panel_height']")).getAttribute("valueAsNumber").equals("5") &&
                driver.findElement(By.xpath("//input[@name='rows.0.panel_height']/../../../../td")).getCssValue("background-color").equals("rgba(224, 224, 224, 1)"));
    }

    public void duplicateBatch(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));
        driver.findElement(By.xpath("(//button[@class='styled_button__Y2Knr'])[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__mwg4x']"), 1));
    }

    public boolean duplicateBatchCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Panel Height']")));

        //////////// читаем данные из оригинального Batch /////////////////////////////
        simulationName = driver.findElement(By.xpath("//label[text()='Base Simulation']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
        panelHeight = driver.findElement(By.xpath("//input[@name='rows.0.panel_height']")).getAttribute("valueAsNumber");
        panelOpacity = driver.findElement(By.xpath("//input[@name='rows.0.panel_opacity']")).getAttribute("valueAsNumber");
        azimuth = driver.findElement(By.xpath("//input[@name='rows.0.azimuth']")).getAttribute("valueAsNumber");
        panelSizeX = driver.findElement(By.xpath("//input[@name='rows.0.panel_size.x']")).getAttribute("valueAsNumber");
        panelSizeY = driver.findElement(By.xpath("//input[@name='rows.0.panel_size.y']")).getAttribute("valueAsNumber");
        initialOffsetX = driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.x']")).getAttribute("valueAsNumber");
        initialOffsetY = driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.y']")).getAttribute("valueAsNumber");
        numberOfPanelsX = driver.findElement(By.xpath("//input[@name='rows.0.panels_number.x']")).getAttribute("valueAsNumber");
        numberOfPanelsY = driver.findElement(By.xpath("//input[@name='rows.0.panels_number.y']")).getAttribute("valueAsNumber");
        spaceBetweenPanelsX = driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.x']")).getAttribute("valueAsNumber");
        spaceBetweenPanelsY = driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.y']")).getAttribute("valueAsNumber");
        fieldSizeX = driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber");
        fieldSizeY = driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber");
        weatherDatasetName = driver.findElement(By.xpath("//td[9]")).getAttribute("outerText");
        productionDatasetName = driver.findElement(By.xpath("//td[10]")).getAttribute("outerText");
        cropName = driver.findElement(By.xpath("//td[11]")).getAttribute("outerText");

        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[@class='styled_button__Y2Knr'])[3]")));
        driver.findElement(By.xpath("(//button[@class='styled_button__Y2Knr'])[3]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Panel Height']")));
        return(driver.findElement(By.xpath("//label[text()='Base Simulation']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals(simulationName) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panel_height']")).getAttribute("valueAsNumber").equals(panelHeight) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panel_opacity']")).getAttribute("valueAsNumber").equals(panelOpacity) &&
            driver.findElement(By.xpath("//input[@name='rows.0.azimuth']")).getAttribute("valueAsNumber").equals(azimuth) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panel_size.x']")).getAttribute("valueAsNumber").equals(panelSizeX) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panel_size.y']")).getAttribute("valueAsNumber").equals(panelSizeY) &&
            driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.x']")).getAttribute("valueAsNumber").equals(initialOffsetX) &&
            driver.findElement(By.xpath("//input[@name='rows.0.initial_offset.y']")).getAttribute("valueAsNumber").equals(initialOffsetY) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panels_number.x']")).getAttribute("valueAsNumber").equals(numberOfPanelsX) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panels_number.y']")).getAttribute("valueAsNumber").equals(numberOfPanelsY) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.x']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsX) &&
            driver.findElement(By.xpath("//input[@name='rows.0.panels_gap.y']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsY) &&
            driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber").equals(fieldSizeX) &&
            driver.findElement(By.xpath("//input[@name='rows.0.field_size.x']")).getAttribute("valueAsNumber").equals(fieldSizeY) &&
            driver.findElement(By.xpath("//td[9]")).getAttribute("outerText").equals(weatherDatasetName) &&
            driver.findElement(By.xpath("//td[10]")).getAttribute("outerText").equals(productionDatasetName) &&
            driver.findElement(By.xpath("//td[11]")).getAttribute("outerText").equals(cropName));
    }

    public boolean deleteBatchCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr styled_red__DzZvD']")));
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr styled_red__DzZvD']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Delete']")));
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__mwg4x']"), 2));
        return (driver.findElements(By.xpath("//div[@class='styled_container__mwg4x']")).size() < 2);
    }

    public boolean deleteEditBatchCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Panel Height']")));
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[text()='Delete'])[2]")));
        driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__mwg4x']"), 2));
        return (driver.findElements(By.xpath("//div[@class='styled_container__mwg4x']")).size() < 2);
    }

    public void runBatch(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Batches']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__Y2Knr']")));
        driver.findElement(By.xpath("//button[@class='styled_button__Y2Knr']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Run Simulation']")));
        driver.findElement(By.xpath("//span[text()='Run Simulation']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Running']")));
        driver.findElement(By.xpath("//a[text()='Batches']/../button[@class='styled_toggle__yOoJv']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Sim 1']")));
        driver.findElement(By.xpath("//a[text()='Sim 1']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5")));

        //////////////////////////////////////////////////////////////////////////////
        int i = 1;
        while (i != 0) {
            try { driver.findElement(By.xpath("//div[@class='styled_spinner__-A5lA spinner-border']")).isDisplayed();}
            catch (Exception e) {i = 0;}
            }
        //System.out.println("done");
        //////////////////////////////////////////////////////////////////////////////
    }

    public void downloadToLocalDirectory (WebDriver driver, WebDriverWait wait) throws InterruptedException {
        /// скачиваем результат батча
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Download']")));
        driver.findElement(By.xpath("//span[text()='Download']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[@class='styled_spinner__yFkql spinner-border']"))));

        /// ждём пока результат батча закачается в локальную директорию
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        for (int i=0; i<20; i++) {
            if (folder.exists() && folder.listFiles().length > 0) {
                if (folder.listFiles()[0].toString().substring
                                ((folder.listFiles()[0].toString().length() - 3), folder.listFiles()[0].toString().length())
                        .equals("zip"))
                {System.out.println("ZIP found"); i=20; }
            }
            Thread.sleep(500);
        }
    }

    public boolean resultBatchCheck(WebDriver driver, WebDriverWait wait) {
        panelHeight = driver.findElement(By.xpath("(//span[text()='Panel Height'])/../span[3]")).getText();
        panelOpacity = driver.findElement(By.xpath("(//span[text()='Panel Opacity'])/../span[3]")).getText();
        panelSize = driver.findElement(By.xpath("(//span[text()='Panel Size'])/../span[3]")).getText();
        initialOffset = driver.findElement(By.xpath("(//span[text()='Initial Offset'])/../span[3]")).getText();
        numberOfPanels = driver.findElement(By.xpath("(//span[text()='Number of Panels'])/../span[3]")).getText();
        spaceBetweenPanels = driver.findElement(By.xpath("(//span[text()='Space Between Panels'])/../span[3]")).getText();
        fieldSize = driver.findElement(By.xpath("(//span[text()='Field Size'])/../span[3]")).getText();
        azimuth = driver.findElement(By.xpath("(//span[text()='Azimuth'])/../span[3]")).getText();
        structureType = driver.findElement(By.xpath("(//span[text()='Structure Type'])/../span[3]")).getText();
        panelTilt = driver.findElement(By.xpath("(//span[text()='Panel Tilt'])/../span[3]")).getText();
        panelTranslation = driver.findElement(By.xpath("(//span[text()='Panel Translation'])/../span[3]")).getText();
        maximalLateralTranslation = driver.findElement(By.xpath("(//span[text()='Maximal Lateral Translation'])/../span[3]")).getText();
        lateralOffset = driver.findElement(By.xpath("(//span[text()='Lateral Offset'])/../span[3]")).getText();
        periodName = driver.findElement(By.xpath("(//span[text()='Period Name'])/../span[3]")).getText();
        startingPoint = driver.findElement(By.xpath("(//span[text()='Starting Point'])/../span[3]")).getText();
        endingPoint = driver.findElement(By.xpath("(//span[text()='Ending Point'])/../span[3]")).getText();
        weatherDatasetType = driver.findElement(By.xpath("(//span[text()='Dataset type'])/../span[3]")).getText();
        weatherSamplingRate = driver.findElement(By.xpath("(//span[text()='Sampling rate'])/../span[3]")).getText();
        weatherTimezone = driver.findElement(By.xpath("(//span[text()='Timezone'])/../span[3]")).getText();
        prodDatasetType = driver.findElement(By.xpath("(//span[text()='Dataset type'])[2]/../span[3]")).getText();
        prodSamplingRate = driver.findElement(By.xpath("(//span[text()='Sampling rate'])[2]/../span[3]")).getText();
        prodTimezone = driver.findElement(By.xpath("(//span[text()='Timezone'])[2]/../span[3]")).getText();

        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='styled_button__WKqbu']")));
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));

        return (driver.findElement(By.xpath("(//span[text()='Panel Height'])/../span[3]")).getText().equals(panelHeight) &&
            driver.findElement(By.xpath("(//span[text()='Panel Opacity'])/../span[3]")).getText().equals(panelOpacity) &&
            driver.findElement(By.xpath("(//span[text()='Panel Size'])/../span[3]")).getText().equals(panelSize) &&
            driver.findElement(By.xpath("(//span[text()='Initial Offset'])/../span[3]")).getText().equals(initialOffset) &&
            driver.findElement(By.xpath("(//span[text()='Number of Panels'])/../span[3]")).getText().equals(numberOfPanels) &&
            driver.findElement(By.xpath("(//span[text()='Space Between Panels'])/../span[3]")).getText().equals(spaceBetweenPanels) &&
            driver.findElement(By.xpath("(//span[text()='Field Size'])/../span[3]")).getText().equals(fieldSize) &&
            driver.findElement(By.xpath("(//span[text()='Azimuth'])/../span[3]")).getText().equals(azimuth) &&
            driver.findElement(By.xpath("(//span[text()='Structure Type'])/../span[3]")).getText().equals(structureType) &&
            driver.findElement(By.xpath("(//span[text()='Panel Tilt'])/../span[3]")).getText().equals(panelTilt) &&
            driver.findElement(By.xpath("(//span[text()='Panel Translation'])/../span[3]")).getText().equals(panelTranslation) &&
            driver.findElement(By.xpath("(//span[text()='Maximal Lateral Translation'])/../span[3]")).getText().equals(maximalLateralTranslation) &&
            driver.findElement(By.xpath("(//span[text()='Lateral Offset'])/../span[3]")).getText().equals(lateralOffset) &&
            driver.findElement(By.xpath("(//span[text()='Period Name'])/../span[3]")).getText().equals(periodName) &&
            driver.findElement(By.xpath("(//span[text()='Starting Point'])/../span[3]")).getText().equals(startingPoint) &&
            driver.findElement(By.xpath("(//span[text()='Ending Point'])/../span[3]")).getText().equals(endingPoint) &&
            driver.findElement(By.xpath("(//span[text()='Dataset type'])/../span[3]")).getText().equals(weatherDatasetType) &&
            driver.findElement(By.xpath("(//span[text()='Sampling rate'])/../span[3]")).getText().equals(weatherSamplingRate) &&
            driver.findElement(By.xpath("(//span[text()='Timezone'])/../span[3]")).getText().equals(weatherTimezone) &&
            driver.findElement(By.xpath("(//span[text()='Dataset type'])[2]/../span[3]")).getText().equals(prodDatasetType) &&
            driver.findElement(By.xpath("(//span[text()='Sampling rate'])[2]/../span[3]")).getText().equals(prodSamplingRate) &&
            driver.findElement(By.xpath("(//span[text()='Timezone'])[2]/../span[3]")).getText().equals(prodTimezone));
    }

    public void unZipFile() {
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        File[] listOfFiles = folder.listFiles();
        String archiveName = listOfFiles[0].getName();
        String zipFilePath = System.getProperty("user.dir") + separator+"download" + separator+ archiveName;
        File destDir = new File("download");

        if (!destDir.exists()) destDir.mkdirs();
        FileInputStream FiS;

        byte[] buffer = new byte[1024];
        try {
            FiS = new FileInputStream(zipFilePath);
            ZipInputStream ZiS = new ZipInputStream(FiS);
            ZipEntry ZE = ZiS.getNextEntry();
            while (ZE != null) {
                String fileName = ZE.getName();
                File newFile = new File(destDir + separator + fileName);

                new File(newFile.getParent()).mkdirs();
                FileOutputStream FoS = new FileOutputStream(newFile);
                int len;
                while ((len = ZiS.read(buffer)) > 0) {
                    FoS.write(buffer, 0, len);
                }
                FoS.close();

                ZiS.closeEntry();
                ZE = ZiS.getNextEntry();
            }

            ZiS.closeEntry();
            ZiS.close();
            FiS.close();
        } catch (IOException e) {
            e.printStackTrace(); }
    }

    public int [] readExcel(String path) throws IOException {
        File file = new File(path);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook wb = new XSSFWorkbook(inputStream);
        XSSFSheet bookmark=wb.getSheet("Analysis");

        XSSFRow row=bookmark.getRow(3);
        XSSFCell cell=row.getCell(2);
        int areaUnderPanels = (int) cell.getNumericCellValue();
        row=bookmark.getRow(3);
        cell=row.getCell(3);
        int areaBetweenPanels = (int) cell.getNumericCellValue();
        row=bookmark.getRow(3);
        cell=row.getCell(4);
        int areaAgrilpv = (int) cell.getNumericCellValue();
        int [] out = {areaUnderPanels, areaBetweenPanels, areaAgrilpv};
        return out;
    }

    public boolean batchResultCompare (WebDriver driver, WebDriverWait wait) throws IOException {
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        File[] listOfFiles = folder.listFiles();
        //String archiveName = listOfFiles[0].getName();
        int resultExel [] = readExcel(System.getProperty("user.dir") + separator +"download" +
                        separator + "Sim 1" + separator + "Period1" + separator + "data.xlsx");
        int areaUnderPanels = resultExel[0];
        int areaBetweenPanels = resultExel[1];
        int areaAgrilPV = resultExel[2];
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody//td[2]")));
        return (Integer.valueOf(driver.findElement(By.xpath("//tbody//td[2]")).getText()).equals(areaBetweenPanels) &&
                Integer.valueOf(driver.findElement(By.xpath("//tbody//td[3]")).getText()).equals(areaUnderPanels) &&
                Integer.valueOf(driver.findElement(By.xpath("//tbody//td[4]")).getText()).equals(areaAgrilPV));
    }
}