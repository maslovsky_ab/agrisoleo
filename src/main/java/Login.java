import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class Login {
    WebDriver driver;
    ChromeOptions options = new ChromeOptions();

    By invalidCredentials = By.xpath("//*[contains(text(),'Incorrect login or password')]");
    By homePageRedirect = By.xpath("//h1[text()='Clients']");
    By dataRequired = By.xpath("//span[@class='SpanError_error__K6mnC']");
    By shortPassword = By.xpath("//span[@class='SpanError_error__K6mnC']");

    public void common(String user, String password) {
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments()).create();
        driver.get("http://admin:48526@agrisoleo.groupbwt.com/login");
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys(user);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public boolean invalidCredentialsMessageDisplayed() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(invalidCredentials));
        return driver.findElement(invalidCredentials).isDisplayed();
    }

    public boolean blankFieldErrorMessageDisplayed() {
        return driver.findElement(dataRequired).isDisplayed();
    }

    public boolean shortPasswordErrorMessageDisplayed() {
        return driver.findElement(shortPassword).getText().equals("Min length is 6 charts");
    }

    public boolean allErrorMessageDisplayed() {
        return (driver.findElements(dataRequired).size() == 2);
    }

    public boolean checkCorrectLogin() {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("123123");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(homePageRedirect));
        return driver.findElement(homePageRedirect).isDisplayed();
    }

}