import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Structures {
    String panelHeight = "2";
    String panelOpacity = "1";
    String panelSizeX = "5";
    String panelSizeY = "1";
    String initialOffsetX = "3";
    String initialOffsetY = "2";
    String numberOfPanelsX = "9";
    String numberOfPanelsY = "14";
    String spaceBetweenPanelsX = "5.5";
    String spaceBetweenPanelsY = "3.5";
    String fieldSizeX = "50";
    String fieldSizeY = "50";
    String resolution = "10";
    String azimuth = "0";
    String structureType = "Fixed";
    String panelTilt = "15";
    String maximalLateralTranslation = "3";
    String lateralOffset = "0";
    String sunPosition = "Fixed";
    String sunPositionAzimuth = "20";
    String sunPositionZenith = "20";


    public void makeNewStructureLoadDefaultValues(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
    }

    public boolean loadDefaultValuesCheck(WebDriver driver, WebDriverWait wait) {
        boolean result;
        if (driver.findElement(By.xpath("//input[@name='panel_height']")).getAttribute("valueAsNumber").equals(panelHeight) &&
                driver.findElement(By.xpath("//label[text()='Panel Opacity']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(panelOpacity) &&
                driver.findElement(By.xpath("//input[@name='panel_x']")).getAttribute("valueAsNumber").equals(panelSizeX) &&
                driver.findElement(By.xpath("//input[@name='panel_y']")).getAttribute("valueAsNumber").equals(panelSizeY) &&
                driver.findElement(By.xpath("//input[@name='initial_offset_x']")).getAttribute("valueAsNumber").equals(initialOffsetX) &&
                driver.findElement(By.xpath("//input[@name='initial_offset_y']")).getAttribute("valueAsNumber").equals(initialOffsetY) &&
                driver.findElement(By.xpath("//input[@name='panels_number_x']")).getAttribute("valueAsNumber").equals(numberOfPanelsX) &&
                driver.findElement(By.xpath("//input[@name='panels_number_y']")).getAttribute("valueAsNumber").equals(numberOfPanelsY) &&
                driver.findElement(By.xpath("//input[@name='panels_gap_x']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsX) &&
                driver.findElement(By.xpath("//input[@name='panels_gap_y']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsY) &&
                driver.findElement(By.xpath("//input[@name='field_size_x']")).getAttribute("valueAsNumber").equals(fieldSizeX) &&
                driver.findElement(By.xpath("//input[@name='field_size_y']")).getAttribute("valueAsNumber").equals(fieldSizeY) &&
                driver.findElement(By.xpath("//input[@name='resolution']")).getAttribute("valueAsNumber").equals(resolution) &&
                driver.findElement(By.xpath("//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(azimuth) &&
                driver.findElement(By.xpath("//h2[text()='Structure Type']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']")).getAttribute("textContent").equals(structureType) &&
                driver.findElement(By.xpath("//label[text()='Panel Tilt']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(panelTilt) &&
                driver.findElement(By.xpath("//input[@name='translation_max_delta']")).getAttribute("valueAsNumber").equals(maximalLateralTranslation) &&
                driver.findElement(By.xpath("//input[@name='translation_init_delta']")).getAttribute("valueAsNumber").equals(lateralOffset) &&
                driver.findElement(By.xpath("//h2[text()='Sun position']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']")).getAttribute("textContent").equals(sunPosition) &&
                driver.findElement(By.xpath("(//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk'])[2]")).getAttribute("textContent").equals(sunPositionAzimuth) &&
                driver.findElement(By.xpath("//label[text()='Zenith']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(sunPositionZenith)
        )
            result = true;
        else result = false;
        return result;
    }

    public void saveNewStructure(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        driver.findElement(By.xpath("//input[@placeholder='Structure Name']")).sendKeys("Structure");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Structure']")));
    }

    public boolean structureCorrectSaveCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = driver.findElement(By.xpath("//p[text()='Structure']")).isDisplayed();
        if (result == true)
        {driver.findElement(By.xpath("//p[text()='Structure']/../..//button[@type='button']")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
            result = loadDefaultValuesCheck(driver, wait);}
        return result;
    }

    public void editStructure(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='Structures']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Structure']/../..//button[@type='button']")));
        driver.findElement(By.xpath("//p[text()='Structure']/../..//button[@type='button']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
        driver.findElement(By.xpath("//input[@placeholder='Structure Name']")).sendKeys("1");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Structure1']")));
        ///////////////////////////////////////////////////////
        driver.findElement(By.xpath("//p[text()='Structure1']/../..//button[@type='button']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
        driver.findElement(By.xpath("//input[@name='panel_height']")).sendKeys(Keys.BACK_SPACE, "1");
        /*driver.findElement(By.xpath("//label[text()='Panel Opacity']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(panelOpacity)
        driver.findElement(By.xpath("//input[@name='panel_x']")).getAttribute("valueAsNumber").equals(panelSizeX)
        driver.findElement(By.xpath("//input[@name='panel_y']")).getAttribute("valueAsNumber").equals(panelSizeY)
        driver.findElement(By.xpath("//input[@name='initial_offset_x']")).getAttribute("valueAsNumber").equals(initialOffsetX)
        driver.findElement(By.xpath("//input[@name='initial_offset_y']")).getAttribute("valueAsNumber").equals(initialOffsetY)
        driver.findElement(By.xpath("//input[@name='panels_number_x']")).getAttribute("valueAsNumber").equals(numberOfPanelsX)
        driver.findElement(By.xpath("//input[@name='panels_number_y']")).getAttribute("valueAsNumber").equals(numberOfPanelsY)
        driver.findElement(By.xpath("//input[@name='panels_gap_x']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsX)
        driver.findElement(By.xpath("//input[@name='panels_gap_y']")).getAttribute("valueAsNumber").equals(spaceBetweenPanelsY)
        driver.findElement(By.xpath("//input[@name='field_size_x']")).getAttribute("valueAsNumber").equals(fieldSizeX)
        driver.findElement(By.xpath("//input[@name='field_size_y']")).getAttribute("valueAsNumber").equals(fieldSizeY)
        driver.findElement(By.xpath("//input[@name='resolution']")).getAttribute("valueAsNumber").equals(resolution)
        driver.findElement(By.xpath("//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(azimuth)
        driver.findElement(By.xpath("//h2[text()='Structure Type']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']")).getAttribute("textContent").equals(structureType)
        driver.findElement(By.xpath("//label[text()='Panel Tilt']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(panelTilt)
        driver.findElement(By.xpath("//input[@name='translation_max_delta']")).getAttribute("valueAsNumber").equals(maximalLateralTranslation)
        driver.findElement(By.xpath("//input[@name='translation_init_delta']")).getAttribute("valueAsNumber").equals(lateralOffset)
        driver.findElement(By.xpath("//h2[text()='Sun position']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']")).getAttribute("textContent").equals(sunPosition)
        driver.findElement(By.xpath("(//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk'])[2]")).getAttribute("textContent").equals(sunPositionAzimuth)
        driver.findElement(By.xpath("//label[text()='Zenith']/..//div[@class='styled_tooltip__i6Njk']")).getAttribute("textContent").equals(sunPositionZenith)*/
        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public void blankFieldsStructure (WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//p[text()='Structure']/../..//button[@type='button']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
        driver.findElement(By.xpath("//input[@placeholder='Structure Name']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panel_height']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panel_x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panel_y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='initial_offset_x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='initial_offset_y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panels_number_x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panels_number_y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panels_gap_x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='panels_gap_y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='field_size_x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='field_size_y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='resolution']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='translation_max_delta']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE));
        driver.findElement(By.xpath("//input[@name='translation_init_delta']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), Keys.TAB);
        Thread.sleep(10000);
        driver.findElement(By.xpath("//button[@type='submit']")).click();

    }

    public boolean blankFieldsStructureCheck (WebDriver driver, WebDriverWait wait) {
        return (
        driver.findElement(By.xpath("//input[@placeholder='Structure Name']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panel_height']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panel_x']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panel_y']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='initial_offset_x']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='initial_offset_y']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panels_number_x']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panels_number_y']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panels_gap_x']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='panels_gap_y']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='field_size_x']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='field_size_y']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='resolution']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='translation_max_delta']/..//../span[text()='This field is required']")).isDisplayed() &&
        driver.findElement(By.xpath("//input[@name='translation_init_delta']/..//../span[text()='This field is required']")).isDisplayed()
        );
    }

    public void addSamplePointsToNewStructure(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Add new point']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s styled_addPoint__54kpn']")).click();
        driver.findElement(By.xpath("//label[text()='Point name']/..//input[@name='name']")).sendKeys("New Sample Point 1");
        driver.findElement(By.xpath("//input[@name='x']")).sendKeys("10");
        driver.findElement(By.xpath("//input[@name='y']")).sendKeys("20");
        driver.findElement(By.xpath("//div[@class='styled_row__eFSxT']//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='New Sample Point 1']")));
    }

    public boolean addSamplePointsToNewStructureCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//input[@placeholder='Structure Name']")).sendKeys("Structure");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Structure']")));
        driver.findElement(By.xpath("//p[text()='Structure']/../..//button[@type='button']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
        return (driver.findElement(By.xpath("(//table[@class='styled_table__7-WLY']//td)[1]")).getText().equals("New Sample Point 1") &&
                driver.findElement(By.xpath("(//table[@class='styled_table__7-WLY']//td)[2]")).getText().equals("10, 20"));
    }

    public void editSamplePoints(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("(//button[@class='styled_button__e6WEB'])[1]")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Add new point']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s styled_addPoint__54kpn']")).click();
        driver.findElement(By.xpath("//label[text()='Point name']/..//input[@name='name']")).sendKeys("New Sample Point 1");
        driver.findElement(By.xpath("//input[@name='x']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='y']")).sendKeys("2");
        driver.findElement(By.xpath("//div[@class='styled_row__eFSxT']//button[@type='submit']")).click();
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='styled_button__AKJGG']")));
        driver.findElement(By.xpath("//button[@class='styled_button__AKJGG']")).click();
        driver.findElement(By.xpath("//input[@name='x']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), "10");
        driver.findElement(By.xpath("//input[@name='y']")).sendKeys(Keys.chord(Keys.CONTROL, Keys.BACK_SPACE), "20");
        driver.findElement(By.xpath("//div[@class='styled_row__eFSxT']//button[@class='styled_button__MiVhp styled_fill__36siS']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='New Sample Point 1']")));
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Structure']")));
    }

    public boolean editSamplePointsCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@class='styled_button__e6WEB']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Add new point']")));
        return (driver.findElement(By.xpath("(//table[@class='styled_table__7-WLY']//td)[1]")).getText().equals("New Sample Point 1") &&
                driver.findElement(By.xpath("(//table[@class='styled_table__7-WLY']//td)[2]")).getText().equals("10, 20"));
    }

}