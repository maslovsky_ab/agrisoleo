import com.codeborne.selenide.selector.ByText;
import jdk.nashorn.internal.parser.DateParser;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.rmi.server.Activation$ActivationSystemImpl_Stub;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import static groovy.xml.Entity.not;

public class Simulations {

    String panelHeight, panelOpacity, panelSize, initialOffset, numberOfPanels, spaceBetweenPanels, fieldSize,azimuth,
            structureType, panelTilt, panelMaxTilt, panelTranslation, maximalLateralTranslation, lateralOffset;
    String periodName;
    String startingPoint;
    String endingPoint;
    String weatherDatasetType, weatherSamplingRate, weatherTimezone;
    String prodDatasetType, prodSamplingRate, prodTimezone;

    public void makeNewSimulation(WebDriver driver, WebDriverWait wait)  {
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Simulations']")));
        driver.findElement(By.xpath("//span[text()='Create new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Run Simulation']")));
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Sim");
        driver.findElement(By.xpath("//span[text()='Structures']/..//div[@class='select__input-container css-iitc3h']")).click();
        driver.findElement(By.xpath("//span[text()='Structures']/..//input[@class='select__input']")).sendKeys("Structure", Keys.RETURN);
        driver.findElement(By.xpath("//span[text()='Crops']/..//input[@class='select__input']")).sendKeys("Crop1", Keys.RETURN);
        driver.findElement(By.xpath("//span[text()='Weather Dataset']/..//input[@class='select__input']")).sendKeys("weather", Keys.RETURN);
        driver.findElement(By.xpath("//span[text()='Electrical Production Dataset']/..//input[@class='select__input']")).sendKeys("prod", Keys.RETURN);

        driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//input[@class='select__input']")).sendKeys("1 hour", Keys.RETURN);
        //driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//input[@class='select__input']")).click();
        //driver.findElement(new ByText("1 hour")).click();

        driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys("description");
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//div[@class='styled_spinner__yFkql spinner-border']"))));
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Sim']")));
    }

    public boolean newSimulationCheck(WebDriver driver, WebDriverWait wait)  {
        //////////// читаем данные из структуры //////////////////////////////
        driver.findElement(By.xpath("//a[text()='Structures']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Structures']/..//..//p[text()='Structure']")));
        driver.findElement(By.xpath("//button[@class='styled_button__e6WEB']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Draw']")));
        panelHeight = driver.findElement(By.xpath("//input[@name='panel_height']")).getAttribute("valueAsNumber")+" m";
        panelOpacity = driver.findElement(By.xpath("//label[text()='Panel Opacity']/..//div[@class='styled_tooltip__i6Njk']")).getText();
        panelSize = driver.findElement(By.xpath("//input[@name='panel_x']")).getAttribute("valueAsNumber") +
                "x"+ driver.findElement(By.xpath("//input[@name='panel_y']")).getAttribute("valueAsNumber") + " m";
        initialOffset = driver.findElement(By.xpath("//input[@name='initial_offset_x']")).getAttribute("valueAsNumber") +
                "x"+ driver.findElement(By.xpath("//input[@name='initial_offset_y']")).getAttribute("valueAsNumber") + " m";
        numberOfPanels = driver.findElement(By.xpath("//input[@name='panels_number_x']")).getAttribute("valueAsNumber") +
                "x"+ driver.findElement(By.xpath("//input[@name='panels_number_y']")).getAttribute("valueAsNumber");
        spaceBetweenPanels = driver.findElement(By.xpath("//input[@name='panels_gap_x']")).getAttribute("valueAsNumber") +
                "x"+ driver.findElement(By.xpath("//input[@name='panels_gap_y']")).getAttribute("valueAsNumber");
        fieldSize = driver.findElement(By.xpath("//input[@name='field_size_x']")).getAttribute("valueAsNumber") +
                "x"+ driver.findElement(By.xpath("//input[@name='field_size_y']")).getAttribute("valueAsNumber") + " m";
        azimuth = driver.findElement(By.xpath("//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk']")).getText() + " º";
        structureType = driver.findElement(By.xpath("//button[@class='styled_button__eZ6Za styled_checked__wHig7']")).getText();
        System.out.println(structureType);
        if (structureType.equalsIgnoreCase("Fixed")) {
            panelTilt = driver.findElement(By.xpath("//label[text()='Panel Tilt']/..//div[@class='styled_tooltip__i6Njk']")).getText();
            System.out.println(panelTilt);
        }
            else {
            panelMaxTilt = driver.findElement(By.xpath("//label[text()='Panel max Tilt']/..//div[@class='styled_tooltip__i6Njk']")).getText();
            System.out.println(panelMaxTilt);
            }
        if (driver.findElement(By.xpath("//label[text()='Panel Translation']//span[@class='styled_checkmark__sdpA6']")).getCssValue("background-color").equals("rgba(86, 161, 121, 1)"))
            panelTranslation = "true";
            else panelTranslation = "false";
        maximalLateralTranslation = driver.findElement(By.xpath("//input[@name='translation_max_delta']")).getAttribute("valueAsNumber") + " m";
        lateralOffset = driver.findElement(By.xpath("//input[@name='translation_init_delta']")).getAttribute("valueAsNumber") + " m";

        //////////// читаем данные из кропов //////////////////////////////
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Crops']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Crop1']/../..//button[@type='button']")));
        driver.findElement(By.xpath("//p[text()='Crop1']/../..//button[@type='button']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody//td")));
        periodName = driver.findElement(By.xpath("//tbody//td")).getText();
        startingPoint = LocalDate.parse(driver.findElement(By.xpath("(//tbody//td)[3]")).getText()).format(DateTimeFormatter.ofPattern("dd MMM", Locale.US));
        endingPoint = LocalDate.parse(driver.findElement(By.xpath("(//tbody//td)[4]")).getText()).format(DateTimeFormatter.ofPattern("dd MMM", Locale.US));

        //////////// читаем данные из weather датасета //////////////////////////////
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class='DatasetsCard_name__yfEN5']")));
        driver.findElement(By.xpath("//p[@class='DatasetsCard_name__yfEN5']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Datasets']")));
        weatherDatasetType = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
        weatherSamplingRate = driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().substring(0, 1)+"H";
        weatherTimezone = driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();

        //////////// читаем данные из production датасета //////////////////////////////
        driver.findElement(By.xpath("//button[@type='button']/../a[text()='Datasets']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class='DatasetsCard_name__yfEN5']")));
        driver.findElement(By.xpath("(//p[@class='DatasetsCard_name__yfEN5'])[2]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Production']")));

        prodDatasetType = driver.findElement(By.xpath("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();
        prodSamplingRate = driver.findElement(By.xpath("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().substring(0, 1)+"H";
        prodTimezone = driver.findElement(By.xpath("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText();

        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Simulations']")));
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[text()='Panel Height']/..//span[@class='styled_text__SNezk'])[2]")));

        /*System.out.println(driver.findElement(By.xpath("(//span[text()='Panel Height']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelHeight));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Panel Opacity']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelOpacity));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Panel Size']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelSize));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Initial Offset']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(initialOffset));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Number of Panels']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(numberOfPanels));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Space Between Panels']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(spaceBetweenPanels));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Field Size']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(fieldSize));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Azimuth']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(azimuth));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Panel Translation']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(panelTranslation));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Maximal Lateral Translation']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(maximalLateralTranslation));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Lateral Offset']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(lateralOffset));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Period Name']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(periodName));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Starting Point']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(startingPoint));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Ending Point']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(endingPoint));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(weatherDatasetType));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(weatherSamplingRate));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(weatherTimezone));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[4]")).getText().equalsIgnoreCase(prodDatasetType));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[4]")).getText().equals(prodSamplingRate));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[4]")).getText().equals(prodTimezone));
        System.out.println(driver.findElement(By.xpath("(//span[text()='Structure Type']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(structureType));*/


        boolean result = (driver.findElement(By.xpath("(//span[text()='Panel Height']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelHeight) &&
                driver.findElement(By.xpath("(//span[text()='Panel Opacity']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelOpacity) &&
                driver.findElement(By.xpath("(//span[text()='Panel Size']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelSize) &&
                driver.findElement(By.xpath("(//span[text()='Initial Offset']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(initialOffset) &&
                driver.findElement(By.xpath("(//span[text()='Number of Panels']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(numberOfPanels) &&
                driver.findElement(By.xpath("(//span[text()='Space Between Panels']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(spaceBetweenPanels) &&
                driver.findElement(By.xpath("(//span[text()='Field Size']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(fieldSize) &&
                driver.findElement(By.xpath("(//span[text()='Azimuth']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(azimuth) &&
                driver.findElement(By.xpath("(//span[text()='Panel Translation']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(panelTranslation) &&
                driver.findElement(By.xpath("(//span[text()='Maximal Lateral Translation']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(maximalLateralTranslation) &&
                driver.findElement(By.xpath("(//span[text()='Lateral Offset']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(lateralOffset) &&
                driver.findElement(By.xpath("(//span[text()='Period Name']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(periodName) &&
                driver.findElement(By.xpath("(//span[text()='Starting Point']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(startingPoint) &&
                driver.findElement(By.xpath("(//span[text()='Ending Point']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(endingPoint) &&
                driver.findElement(By.xpath("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(weatherDatasetType) &&
                driver.findElement(By.xpath("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(weatherSamplingRate) &&
                driver.findElement(By.xpath("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(weatherTimezone) &&
                driver.findElement(By.xpath("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[4]")).getText().equalsIgnoreCase(prodDatasetType) &&
                driver.findElement(By.xpath("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[4]")).getText().equals(prodSamplingRate) &&
                driver.findElement(By.xpath("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[4]")).getText().equals(prodTimezone)) &&
                driver.findElement(By.xpath("(//span[text()='Structure Type']/..//span[@class='styled_text__SNezk'])[2]")).getText().equalsIgnoreCase(structureType);

        if (result == true){
            if (structureType.equalsIgnoreCase("Fixed")) {
                result = driver.findElement(By.xpath("(//span[text()='Panel Tilt']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelTilt);
            }
            else result = driver.findElement(By.xpath("(//span[text()='Panel max Tilt']/..//span[@class='styled_text__SNezk'])[2]")).getText().equals(panelMaxTilt);
        }
        return  result;

    }

    public void editSimulation(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Sim']")));
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Run Simulation']")));
        driver.findElement(By.xpath("//input[@name='resolution']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "5");
        driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys("2");
        driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//input[@class='select__input']")).click();
        driver.findElement(new ByText("2 hour")).click();
        driver.findElement(By.xpath("//input[@name='max_scale_value']")).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE), "5000");
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("2");

        driver.findElement(By.xpath("//span[text()='Save']")).click();
        wait.until(ExpectedConditions.attributeContains(By.xpath("//button[@type='submit']"), "disabled", "true"));
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Sim2']")));
    }

    public boolean editSimulationCheck(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));
        /*System.out.println(driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("Sim2"));
        System.out.println(driver.findElement(By.xpath("//input[@name='resolution']")).getAttribute("defaultValue").equals("5"));
        System.out.println(driver.findElement(By.xpath("//textarea[@name='description']")).getAttribute("defaultValue").equals("description2"));
        System.out.println(driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("2 hour"));
        System.out.println(driver.findElement(By.xpath("//input[@name='max_scale_value']")).getAttribute("defaultValue").equals("5000"));*/
        return(driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("Sim2") &&
                driver.findElement(By.xpath("//input[@name='resolution']")).getAttribute("defaultValue").equals("5") &&
                driver.findElement(By.xpath("//textarea[@name='description']")).getAttribute("defaultValue").equals("description2") &&
                driver.findElement(By.xpath("//label[text()='Simulation Frequency']/..//div[@class='select__single-value css-1dimb5e-singleValue']")).getText().equals("2 hour") &&
                driver.findElement(By.xpath("//input[@name='max_scale_value']")).getAttribute("defaultValue").equals("5000"));
    }

    public void duplicateSimulation(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Sim']")));
        driver.findElement(By.xpath("(//button[@class='styled_button__WKqbu'])[2]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
    }

    public boolean duplicateSimulationCheck(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));

        panelHeight = driver.findElement(By.xpath("//span[text()='Panel Height']/../span[3]")).getText();
        panelOpacity = driver.findElement(By.xpath("//span[text()='Panel Opacity']/../span[3]")).getText();
        panelSize = driver.findElement(By.xpath("//span[text()='Panel Size']/../span[3]")).getText();
        initialOffset = driver.findElement(By.xpath("//span[text()='Initial Offset']/../span[3]")).getText();
        numberOfPanels = driver.findElement(By.xpath("//span[text()='Number of Panels']/../span[3]")).getText();
        spaceBetweenPanels = driver.findElement(By.xpath("//span[text()='Space Between Panels']/../span[3]")).getText();
        fieldSize = driver.findElement(By.xpath("//span[text()='Field Size']/../span[3]")).getText();
        azimuth = driver.findElement(By.xpath("//span[text()='Azimuth']/../span[3]")).getText();
        structureType = driver.findElement(By.xpath("//span[text()='Structure Type']/../span[3]")).getText();
        panelMaxTilt = driver.findElement(By.xpath("//span[text()='Panel max Tilt']/../span[3]")).getText();
        panelTranslation = driver.findElement(By.xpath("//span[text()='Panel Translation']/../span[3]")).getText();
        maximalLateralTranslation = driver.findElement(By.xpath("//span[text()='Maximal Lateral Translation']/../span[3]")).getText();
        lateralOffset = driver.findElement(By.xpath("//span[text()='Lateral Offset']/../span[3]")).getText();

        periodName = driver.findElement(By.xpath("//span[text()='Period Name']/../span[3]")).getText();
        startingPoint = driver.findElement(By.xpath("//span[text()='Starting Point']/../span[3]")).getText();
        endingPoint = driver.findElement(By.xpath("//span[text()='Ending Point']/../span[3]")).getText();

        weatherDatasetType = driver.findElement(By.xpath("//span[text()='Dataset type']/../span[3]")).getText();
        weatherSamplingRate = driver.findElement(By.xpath("//span[text()='Sampling rate']/../span[3]")).getText();
        weatherTimezone = driver.findElement(By.xpath("//span[text()='Timezone']/../span[3]")).getText();

        prodDatasetType = driver.findElement(By.xpath("(//span[text()='Dataset type']/../span[3])[2]")).getText();
        prodSamplingRate = driver.findElement(By.xpath("(//span[text()='Sampling rate']/../span[3])[2]")).getText();
        prodTimezone = driver.findElement(By.xpath("(//span[text()='Timezone']/../span[3])[2]")).getText();

        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
        driver.findElement(By.xpath("(//button[@class='styled_button__WKqbu'])[3]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));

        return(driver.findElement(By.xpath("//span[text()='Panel Height']/../span[3]")).getText().equals(panelHeight) &&
        driver.findElement(By.xpath("//span[text()='Panel Opacity']/../span[3]")).getText().equals(panelOpacity) &&
        driver.findElement(By.xpath("//span[text()='Panel Size']/../span[3]")).getText().equals(panelSize) &&
        driver.findElement(By.xpath("//span[text()='Initial Offset']/../span[3]")).getText().equals(initialOffset) &&
        driver.findElement(By.xpath("//span[text()='Number of Panels']/../span[3]")).getText().equals(numberOfPanels) &&
        driver.findElement(By.xpath("//span[text()='Space Between Panels']/../span[3]")).getText().equals(spaceBetweenPanels) &&
        driver.findElement(By.xpath("//span[text()='Field Size']/../span[3]")).getText().equals(fieldSize) &&
        driver.findElement(By.xpath("//span[text()='Azimuth']/../span[3]")).getText().equals(azimuth) &&
        driver.findElement(By.xpath("//span[text()='Structure Type']/../span[3]")).getText().equals(structureType) &&
        driver.findElement(By.xpath("//span[text()='Panel max Tilt']/../span[3]")).getText().equals(panelMaxTilt) &&
        driver.findElement(By.xpath("//span[text()='Panel Translation']/../span[3]")).getText().equals(panelTranslation) &&
        driver.findElement(By.xpath("//span[text()='Maximal Lateral Translation']/../span[3]")).getText().equals(maximalLateralTranslation) &&
        driver.findElement(By.xpath("//span[text()='Lateral Offset']/../span[3]")).getText().equals(lateralOffset) &&
        driver.findElement(By.xpath("//span[text()='Period Name']/../span[3]")).getText().equals(periodName) &&
        driver.findElement(By.xpath("//span[text()='Starting Point']/../span[3]")).getText().equals(startingPoint) &&
        driver.findElement(By.xpath("//span[text()='Ending Point']/../span[3]")).getText().equals(endingPoint) &&
        driver.findElement(By.xpath("//span[text()='Dataset type']/../span[3]")).getText().equals(weatherDatasetType) &&
        driver.findElement(By.xpath("//span[text()='Sampling rate']/../span[3]")).getText().equals(weatherSamplingRate) &&
        driver.findElement(By.xpath("//span[text()='Timezone']/../span[3]")).getText().equals(weatherTimezone) &&
        driver.findElement(By.xpath("(//span[text()='Dataset type']/../span[3])[2]")).getText().equals(prodDatasetType) &&
        driver.findElement(By.xpath("(//span[text()='Sampling rate']/../span[3])[2]")).getText().equals(prodSamplingRate) &&
        driver.findElement(By.xpath("(//span[text()='Timezone']/../span[3])[2]")).getText().equals(prodTimezone));
    }

    public boolean deleteSimulationCheck(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu styled_red__um3nH']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 2));
        return (driver.findElements(By.xpath("//div[@class='styled_container__AE6Nl']")).size() < 2);
    }

    public boolean deleteEditSimulationCheck(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='styled_button__WKqbu']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s styled_deleteButton__I2+TS']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']")));
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']")).click();
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
        return (driver.findElements(By.xpath("//div[@class='styled_container__AE6Nl']")).size() < 2);
    }

}