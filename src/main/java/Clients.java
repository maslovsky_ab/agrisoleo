import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Clients {
    String user = "admin";
    String password = "123123";
    By userInput = By.xpath("//input[@name='username']");
    By passwordInput = By.xpath("//input[@name='password']");
    By submitButton = By.xpath("//button[@type='submit']");

    public void visitPage(WebDriver driver, WebDriverWait wait) {
        driver.get("http://admin:48526@agrisoleo.groupbwt.com/login");
        //driver.get("https://admin:48526@app.agrisoleo.fr/login");
        wait.until(ExpectedConditions.visibilityOfElementLocated(userInput));

        driver.findElement(userInput).sendKeys(user);
        driver.findElement(passwordInput).sendKeys(password);
        driver.findElement(submitButton).click();
        driver.findElement(userInput).sendKeys(user);
        driver.findElement(passwordInput).sendKeys(password);
        driver.findElement(submitButton).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='styled_container__0KwwM']")));
    }

    public void makeNewClient(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("autoTest");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("autoTest@autoTest.com");
        driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys("autoTest description");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest']")));
    }

    public boolean makeNewClientCheck(WebDriver driver, WebDriverWait wait) {
        boolean result;
        if (driver.findElement(By.xpath("//a[text()='autoTest']")).isDisplayed()) { result = true; }
        else {result = false;}
        driver.findElement(By.xpath("//a[text()='autoTest']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[text()='Edit client']")));
        if (driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("autoTest") &&
                driver.findElement(By.xpath("//input[@name='email']")).getAttribute("defaultValue").equalsIgnoreCase("autoTest@autoTest.com") &&
                driver.findElement(By.xpath("//textarea[@name='description']")).getText().equals("autoTest description") &&
                result == true) {result = true;}
        else result = false;
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        return result;
    }

    public boolean makeNewClientBlankNameFieldCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Name']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean makeNewClientBlankEmailFieldCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Contact Email']/../span[text()='This field is required']")).isDisplayed();
    }

    public boolean makeNewClientMisspelledEmailCheck(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("1#1.com");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return driver.findElement(By.xpath("//label[text()='Contact Email']/../span[text()='email must be a valid email']")).isDisplayed();
    }

    public void editClient(WebDriver driver, WebDriverWait wait) {
        driver.findElement(By.xpath("//a[text()='autoTest']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("n");
        driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys("1");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest1']")));
        }

    public boolean editClientCheck(WebDriver driver, WebDriverWait wait) {
        boolean result = false;
        if (driver.findElement(By.xpath("//a[text()='autoTest1']")).isDisplayed()) { result = true; }
        else {return false;}
        driver.findElement(By.xpath("//a[text()='autoTest1']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[text()='Edit client']")));
        if (driver.findElement(By.xpath("//input[@name='name']")).getAttribute("defaultValue").equals("autoTest1") &&
                driver.findElement(By.xpath("//input[@name='email']")).getAttribute("defaultValue").equalsIgnoreCase("autoTest@autoTest.con") &&
                driver.findElement(By.xpath("//textarea[@name='description']")).getText().equals("autoTest description1") &&
                result == true) {result = true;}
        else result = false;
        driver.findElement(By.xpath("//button[@class='styled_button__MiVhp styled_text__BX30s']")).click();
        driver.findElement(By.xpath("//a[text()='autoTest1']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Edit']")).click();
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(Keys.BACK_SPACE);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest']")));
        return result;
        }

    public boolean deleteClientCheck(WebDriver driver, WebDriverWait wait){
        driver.findElement(By.xpath("//a[text()='autoTest']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Delete']")).click();
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//a[text()='autoTest']"))));
        return driver.findElements(By.xpath("//a[text()='autoTest1']")).size() == 0;
    }


    public void deleteClient(WebDriver driver, WebDriverWait wait) {
        driver.get("http://agrisoleo.groupbwt.com/clients");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='autoTest']")));
        driver.findElement(By.xpath("//a[text()='autoTest']/..//..//..//button[@class='styled_toggle__6Bo8e']")).click();
        driver.findElement(By.xpath("//a[text()='Delete']")).click();
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
    }

}