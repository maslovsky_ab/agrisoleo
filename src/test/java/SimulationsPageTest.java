import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.time.Duration;


@Tag("Simulations")
public class SimulationsPageTest {
    ChromeOptions options = new ChromeOptions();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    Projects projects = new Projects();
    Clients clients = new Clients();
    Structures structures = new Structures();
    Crops crops = new Crops();
    Datasets datasets = new Datasets();
    Simulations simulations = new Simulations();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
        /*clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
        structures.saveNewStructure(driver, wait);
        crops.makeNewCrop(driver, wait);
        datasets.makeNewWeatherDataset(driver, wait);
        datasets.makeNewProductionDataset(driver, wait);*/
        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='autoTest Project new']")));
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Simulations']")));
        driver.findElement(By.xpath("//a[text()='Simulations']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Simulations']")));

    }

    @AfterEach
    public void afterTest() {
        //clients.deleteClient(driver, wait);
        //driver.quit();
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "make new Simulation")
    @Step("в новой симуляции отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void makeNewSimulation()  {
        simulations.makeNewSimulation(driver, wait);
        Assert.assertTrue(simulations.newSimulationCheck(driver, wait));
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "edit Simulation")
    @Step("в отредактированной симуляции отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void editSimulation() throws InterruptedException {
        simulations.makeNewSimulation(driver, wait);
        simulations.editSimulation(driver, wait);
        Assert.assertTrue(simulations.editSimulationCheck(driver, wait));
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "duplicate Simulation")
    @Step("в копии симуляции отображаются те же значения из Структур, Кропов и Датасетов")
    @Test
    public void duplicateSimulation() throws InterruptedException {
        simulations.makeNewSimulation(driver, wait);
        simulations.duplicateSimulation(driver, wait);
        Assert.assertTrue(simulations.duplicateSimulationCheck(driver, wait));
    }
    @Epic(value = "Simulations-page")
    @Feature(value = "delete Simulation")
    @Step("симуляция удаляется со страницы Симуляций")
    @Test
    public void deleteSimulation() throws InterruptedException {
        simulations.makeNewSimulation(driver, wait);
        simulations.duplicateSimulation(driver, wait);
        Assert.assertTrue(simulations.deleteSimulationCheck(driver, wait));
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "delete Simulation")
    @Step("симуляция удаляется со страницы редактирования Симуляции")
    @Test
    public void deleteEditSimulation() throws InterruptedException {
        simulations.makeNewSimulation(driver, wait);
        simulations.duplicateSimulation(driver, wait);
        Assert.assertTrue(simulations.deleteEditSimulationCheck(driver, wait));
    }

}


