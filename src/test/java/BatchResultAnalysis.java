import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import static java.io.File.separator;

@Tag("Analysis")
public class BatchResultAnalysis {
    Clients clients = new Clients();
    Projects projects = new Projects();
    Structures structures = new Structures();
    Crops crops = new Crops();
    Datasets datasets = new Datasets();
    Simulations simulations = new Simulations();
    Batch batch = new Batch();

    @Epic(value = "Comparing Batch results on page with results in downloaded Excel")
    @Test
    public void batchResultAnalysis() throws Throwable {
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("download.default_directory", System.getProperty("user.dir")+ separator+"download");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        System.setProperty("download.default_directory", System.getProperty("user.dir")+ separator+"path"+ separator+"to"+ separator+"download");

        //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
        WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));

        clients.visitPage(driver, wait);
        clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
        structures.saveNewStructure(driver, wait);
        crops.makeNewCrop(driver, wait);
        datasets.makeNewWeatherDataset(driver, wait);
        datasets.makeNewProductionDataset(driver, wait);
        simulations.makeNewSimulation(driver, wait);
        batch.makeNewBatch(driver, wait);
        batch.runBatch(driver, wait);
        batch.downloadToLocalDirectory(driver, wait);
        batch.unZipFile();

        Assert.assertTrue(batch.batchResultCompare(driver, wait));

        FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+ separator+"download"));
        clients.deleteClient(driver, wait);
    }

}


