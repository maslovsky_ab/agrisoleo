import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Array;
import java.time.Duration;
import java.util.List;


@Tag("Structures")
public class StructuresPageTest {
    ChromeOptions options = new ChromeOptions();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    Projects projects = new Projects();
    Clients clients = new Clients();
    Structures structures = new Structures();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
        clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
    }

    @AfterEach
    public void afterTest() {
        //projects.driver.quit();
        clients.deleteClient(driver, wait);
    }

    @Epic(value = "Structures-page")
    @Feature(value = "make new Structure")
    @Step("значения переменных в структуре соответствуют дефолтным значениям")
    @Test
    public void makeNewStructureDefaultValues() {
        structures.makeNewStructureLoadDefaultValues(driver, wait);
        Assert.assertTrue(structures.loadDefaultValuesCheck(driver, wait));
    }

    @Epic(value = "Structures-page")
    @Feature(value = "make new Structure")
    @Step("структура сохраняется и отображается; дефолтные значения в структуре сохраняются")
    @Test
    public void makeNewStructure() {
        structures.saveNewStructure(driver, wait);
        Assert.assertTrue(structures.structureCorrectSaveCheck(driver, wait));
    }

    ////////////////////////////////////////////////////////////
    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("структура сохраняется и отображается после редактирования")
    @Test
    public void editStructure() {
        structures.saveNewStructure(driver, wait);
        structures.editStructure(driver, wait);
        //Assert.assertTrue(structures.structureCorrectSaveCheck(driver, wait));
    }
////////////////////////////////////////////////////////////////

    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("для пустых полей отображаются сообщения об ошибке")
    @Test
    public void blankFieldsStructure() throws InterruptedException {
        structures.saveNewStructure(driver, wait);
        structures.blankFieldsStructure(driver, wait);
        Assert.assertTrue(structures.blankFieldsStructureCheck(driver, wait));
    }

    ////////////////////////////////////// тут написал репорт, работа фронта не совпадает с требованиями бэка
    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("для недопустимых значений отображаются сообщения об ошибке")
    @Test
    public void incorrectValuesFieldsStructure() throws InterruptedException {
        structures.saveNewStructure(driver, wait);
        structures.blankFieldsStructure(driver, wait);
        Assert.assertTrue(structures.blankFieldsStructureCheck(driver, wait));
    }

    @Epic(value = "Structures-page")
    @Feature(value = "add Sample Points to Structure")
    @Step("Sample Points добавляются и отображаются")
    @Test
    public void addSamplePointsToNewStructure() {
        structures.addSamplePointsToNewStructure(driver, wait);
        Assert.assertTrue(structures.addSamplePointsToNewStructureCheck(driver, wait));
    }

    @Epic(value = "Structures-page")
    @Feature(value = "edit Sample Points")
    @Step("исправленные значения Sample Points отображаются")
    @Test
    public void editSamplePoints() throws InterruptedException {
        structures.saveNewStructure(driver, wait);
        structures.editSamplePoints(driver, wait);
        Assert.assertTrue(structures.editSamplePointsCheck(driver, wait));
    }

}
