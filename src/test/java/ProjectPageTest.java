import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


@Tag("Projects")
public class ProjectPageTest {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments()).create();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    Projects projects = new Projects();
    Clients clients = new Clients();
    Login login = new Login();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
        clients.makeNewClient(driver, wait);
    }

    @AfterEach
    public void afterTest() {
        //projects.driver.quit();
        clients.deleteClient(driver, wait);
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Test
    public void makeNewProject() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.makeNewProjectCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Имя' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankNameField() {
        Assert.assertTrue(projects.makeNewProjectBlankNameFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Latitude' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankLatitudeField() {
        Assert.assertTrue(projects.makeNewProjectBlankLatitudeFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Longitude' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankLongitudeField() {
        Assert.assertTrue(projects.makeNewProjectBlankLongitudeFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Latitude <-90 - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLatitudeLess90() {
        Assert.assertTrue(projects.makeNewProjectLatitudeLess90Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Latitude >90 - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLatitudeMore90() {
        Assert.assertTrue(projects.makeNewProjectLatitudeMore90Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Longitude <-180 - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLongitudeLess180() {
        Assert.assertTrue(projects.makeNewProjectLongitudeLess180Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Longitude >180 - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLongitudeMore180() {
        Assert.assertTrue(projects.makeNewProjectLongitudeMore180Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Test
    public void editProject() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Name' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankNameField() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectBlankNameFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Latitude' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankLatitudeField() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectBlankLatitudeFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Longitude' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankLongitudeField() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectBlankLongitudeFieldCheck(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Latitude <-90 - отображение сообщения об ошибке")
    @Test
    public void editProjectLatitudeLess90() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectLatitudeLess90Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Latitude >90 - отображение сообщения об ошибке")
    @Test
    public void editProjectLatitudeMore90() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectLatitudeMore90Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Longitude <-180 - отображение сообщения об ошибке")
    @Test
    public void editProjectLongitudeLess180() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectLongitudeLess180Check(driver, wait));
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Longitude >180 - отображение сообщения об ошибке")
    @Test
    public void editProjectLongitudeMore180() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.editProjectLongitudeMore180Check(driver, wait));
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project")
    @Test
    public void deleteProject() {
        projects.makeNewProject(driver, wait);
        Assert.assertTrue(projects.deleteProjectCheck(driver, wait));
    }

}
