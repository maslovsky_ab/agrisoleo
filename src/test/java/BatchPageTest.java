import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

@Tag("Batch")
public class BatchPageTest {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));

//    Projects projects = new Projects();
    Clients clients = new Clients();
    Structures structures = new Structures();
    Crops crops = new Crops();
    Datasets datasets = new Datasets();
    Simulations simulations = new Simulations();
    Batch batch = new Batch();

    @BeforeEach
    public void beforeTest() {
        //clients.visitPage(driver, wait);
        /*clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
        structures.saveNewStructure(driver, wait);
        crops.makeNewCrop(driver, wait);
        datasets.makeNewWeatherDataset(driver, wait);
        datasets.makeNewProductionDataset(driver, wait);
        simulations.makeNewSimulation(driver, wait);*/

        driver.findElement(By.xpath("//a[text()='autoTest']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='autoTest Project new']")));
        driver.findElement(By.xpath("//a[text()='autoTest Project new']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Simulations']")));
    }

    @AfterEach
    public void afterTest() {
        //clients.deleteClient(driver, wait);
        //driver.quit();
    }

    @Epic(value = "Batch-page")
    @Feature(value = "make new Batch")
    @Step("в новом Batch отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void makeNewBatch()  {
        batch.makeNewBatch(driver, wait);
        Assert.assertTrue(batch.newBatchCheck(driver, wait));
    }

    @Epic(value = "Batch-page")
    @Feature(value = "edit Batch")
    @Step("в отредактированный Batch подтягиваются и сохраняются значения из отредаткиторванных Структур, Кропов и Датасетов")
    @Test
    public void editBatch() {
        //batch.makeNewBatch(driver, wait);
        batch.editBatch(driver, wait);
        Assert.assertTrue(batch.editBatchCheck(driver, wait));
    }

    @Epic(value = "Batch-page")
    @Feature(value = "duplicate Batch")
    @Step("в копии Batchа такие же значения как в оригинальном Batchе")
    @Test
    public void duplicateBatch() {
        //batch.makeNewBatch(driver, wait);
        batch.duplicateBatch(driver, wait);
        Assert.assertTrue(batch.duplicateBatchCheck(driver, wait));
    }

    @Epic(value = "Batch-page")
    @Feature(value = "delete Batch")
    @Step("удаляем копию Batchа на странице Batchей")
    @Test
    public void deleteBatch() {
        //batch.makeNewBatch(driver, wait);
        batch.duplicateBatch(driver, wait);
        Assert.assertTrue(batch.deleteBatchCheck(driver, wait));
    }

    @Epic(value = "Batch-page")
    @Feature(value = "delete Batch")
    @Step("удаляем Batch на странице редактирования Batchа")
    @Test
    public void deleteEditBatch() {
        batch.makeNewBatch(driver, wait);
        batch.duplicateBatch(driver, wait);
        Assert.assertTrue(batch.deleteEditBatchCheck(driver, wait));
    }

    @Epic(value = "Batch-page")
    @Feature(value = "run Batch")
    @Step("запускаем Batch на просчет и сравниваем данные результаты с эталонными данными")
    @Test
    public void runBatch() {
        //batch.makeNewBatch(driver, wait);
        batch.runBatch(driver, wait);
        Assert.assertTrue(batch.resultBatchCheck(driver, wait));
    }

}


