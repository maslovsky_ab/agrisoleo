import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Login")
public class LogInTest {
    Login logIn = new Login();

    @After
    public void afterTest() {
        logIn.driver.quit();
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пользователь отсутствует в базе")
    @Test
    public void loginWithIncorrectUserName () {
        logIn.common ("admin1", "123123");
        Assert.assertTrue(logIn.invalidCredentialsMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("некорректный пароль существующего юзера")
    @Test
    public void loginWithIncorrectUserPassword () {
        logIn.common ("admin", "1123123123");
        Assert.assertTrue(logIn.invalidCredentialsMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пустое поле имени пользователя")
    @Test
    public void loginWithBlankUsernameField () {
        logIn.common ("", "11");
        Assert.assertTrue(logIn.blankFieldErrorMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пустое поле пароля")
    @Test
    public void loginWithBlankPasswordField() {
        logIn.common ("admin", "");
        Assert.assertTrue(logIn.blankFieldErrorMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("короткий пароль")
    @Test
    public void loginWithShortPassword() {
        logIn.common ("admin", "123");
        Assert.assertTrue(logIn.shortPasswordErrorMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пустые поля имя пользователя и пароля")
    @Test
    public void loginErrorMessageDisplaysForAllFieldsWithError () {
        logIn.common ("", "");
        Assert.assertTrue(logIn.allErrorMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Correct Login")
    @Test
    public void loginCorrect () throws InterruptedException {
        logIn.common ("admin", "123123");
        Assert.assertTrue(logIn.checkCorrectLogin());
    }
}
