import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


@Tag("Crops")
public class CropsPageTest {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    Projects projects = new Projects();
    Clients clients = new Clients();
    Crops crops = new Crops();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
        clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient(driver, wait);
        //driver.quit();
    }

    @Epic(value = "Crops-page")
    @Feature(value = "make new Crop")
    @Step("введенные значения в новом кропе отображаются и сохраняются")
    @Test
    @Order(1)
    public void makeNewCrop() {
        crops.makeNewCrop(driver, wait);
        Assert.assertTrue(crops.newCropCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "new Crop")
    @Step("отображение сообщений об ошибках")
    @Test
    @Order(2)
    public void newCropErrorShow() {
        Assert.assertTrue(crops.newCropErrorShowCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "edit Crop")
    @Step("новые значения в отредактированном кропе отображаются и сохраняются")
    @Test
    @Order(3)
    public void editCrop() {
        crops.makeNewCrop(driver, wait);
        crops.editCrop(driver, wait);
        Assert.assertTrue(crops.editCropCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "edit Crop")
    @Step("отображение сообщений об ошибках")
    @Test
    @Order(4)
    public void editCropErrorShow() {
        crops.makeNewCrop(driver, wait);
        Assert.assertTrue(crops.editCropErrorShowCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "duplicate Crop")
    @Step("отображение сообщений об ошибках")
    @Test
    @Order(5)
    public void duplicateCrop() throws InterruptedException {
        crops.makeNewCrop(driver, wait);
        Assert.assertTrue(crops.duplicateCropCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "duplicate Crop")
    @Test
    @Order(6)
    public void deleteCrop() {
        crops.makeNewCrop(driver, wait);
        Assert.assertTrue(crops.deleteCropCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "delete Period for New Crop")
    @Test
    @Order(7)
    public void deletePeriodNewCrop() {
        Assert.assertTrue(crops.deletePeriodNewCropCheck(driver, wait));
    }

    @Epic(value = "Crops-page")
    @Feature(value = "delete Period for existing Crop")
    @Test
    @Order(8)
    public void deletePeriodExistingCrop() {
        crops.makeNewCrop(driver, wait);
        Assert.assertTrue(crops.deletePeriodExistingCropCheck(driver, wait));
    }

}
