import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


@Tag("Datasets")
public class DatasetsPageTest {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    Projects projects = new Projects();
    Clients clients = new Clients();
    Datasets datasets = new Datasets();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
        clients.makeNewClient(driver, wait);
        projects.makeNewProject(driver, wait);
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient(driver, wait);
        //driver.quit();
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "make new Weather Dataset")
    @Step("введенные значения в новом датасете отображаются и сохраняются")
    @Test
    public void makeNewWeatherDataset() {
        datasets.makeNewWeatherDataset(driver, wait);
        Assert.assertTrue(datasets.newWeatherDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "make new Production Dataset")
    @Step("введенные значения в новом датасете отображаются и сохраняются")
    @Test
    public void makeNewProductionDataset() {
        datasets.makeNewProductionDataset(driver, wait);
        Assert.assertTrue(datasets.newProductionDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "error messages displaying")
    @Step("отображаются сообщения об ошибках при сохранении пустого датасета")
    @Test
    public void makeNewBlankDataset() {
        datasets.makeNewBlankDataset(driver, wait);
        Assert.assertTrue(datasets.newBlankDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "error messages displaying")
    @Step("отображаются сообщения при загрузке датасет-Экселя с ошибкой")
    @Test
    public void makeNewDatasetIncorrectExcel() {
        datasets.makeNewDatasetIncorrectExcel(driver, wait);
        Assert.assertTrue(datasets.makeNewDatasetIncorrectExcelCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "edit Weather Dataset")
    @Step("исправленные значения в датасете отображаются и сохраняются после редактирования")
    @Test
    public void editWeatherDataset() throws InterruptedException {
        datasets.makeNewWeatherDataset(driver, wait);
        datasets.editWeatherDataset(driver, wait);
        Assert.assertTrue(datasets.editWeatherDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "edit Production Dataset")
    @Step("исправленные значения в датасете отображаются и сохраняются после редактирования")
    @Test
    public void editProductionDataset() {
        datasets.makeNewProductionDataset(driver, wait);
        datasets.editProductionDataset(driver, wait);
        Assert.assertTrue(datasets.editProductionDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "error messages displaying")
    @Step("отображаются сообщения об ошибках при редактировании датасета")
    @Test
    public void editDatasetMessages() throws InterruptedException {
        datasets.makeNewProductionDataset(driver, wait);
        //datasets.editDatasetMessages(driver, wait);
        Assert.assertTrue(datasets.editDatasetMessagesCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "duplicate dataset")
    @Step("датасет скопировался с тем же именем и с теми же значениями")
    @Test
    public void duplicateDataset() throws InterruptedException {
        datasets.makeNewProductionDataset(driver, wait);
        //datasets.duplicateDataset(driver, wait);
        Assert.assertTrue(datasets.duplicateDatasetCheck(driver, wait));
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "delete dataset")
    @Step("датасет удаляется")
    @Test
    public void deleteDataset() throws InterruptedException {
        datasets.makeNewProductionDataset(driver, wait);
        Assert.assertTrue(datasets.deleteDatasetCheck(driver, wait));
    }
}
