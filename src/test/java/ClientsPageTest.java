import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

@Tag("Clients")
public class ClientsPageTest {
    ChromeOptions options = new ChromeOptions();
    //WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments()).create();
    WebDriver driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    Clients clients = new Clients();

    @BeforeEach
    public void beforeTest() {
        clients.visitPage(driver, wait);
    }

    @AfterEach
    public void afterTest() {
        //clients.driver.quit();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Test
    public void makeNewClient() {
        clients.makeNewClient(driver, wait);
        Assert.assertTrue(clients.makeNewClientCheck(driver, wait));
        clients.deleteClient(driver, wait);
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("пустое поле 'Имя' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientBlankNameField() {
        Assert.assertTrue(clients.makeNewClientBlankNameFieldCheck(driver, wait));
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("пустое поле 'Имя' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientBlankEmailField() {
        Assert.assertTrue(clients.makeNewClientBlankEmailFieldCheck(driver, wait));
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("пустое поле 'Email' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientMisspelledEmail() {
        Assert.assertTrue(clients.makeNewClientMisspelledEmailCheck(driver, wait));
    }

    @Epic(value = "Clients-page")
    @Feature(value = "edit Client")
    @Test
    public void editClient() {
        clients.makeNewClient(driver, wait);
        clients.editClient(driver, wait);
        Assert.assertTrue(clients.editClientCheck(driver, wait));
        clients.deleteClient(driver, wait);
    }

    @Epic(value = "Clients-page")
    @Feature(value = "delete Client")
    @Test
    public void deleteClient() {
        clients.makeNewClient(driver, wait);
        Assert.assertTrue(clients.deleteClientCheck(driver, wait));
    }

}
